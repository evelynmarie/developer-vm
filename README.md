developer-vm
============

VirtualBox Image with GITLAB Developer setup. 


===========

### Contain GITLAB setup Jun 30, vim, git. 

### 1. Get VM Image 

* Clone this repo or use [Dropbox link](http://dl.dropbox.com/u/71600828/GITLAB.ova). Its ~ 1.5GB in both cases.
* Import GITLAB.ova into your VirtualBox Setup

### 2. Use ssh access to dev vm

```
ssh gitlab@192.168.0.100
password: 5iveL!fe
```

### 3. Get latest code

```
cd gitlab
git pull origin master
cp config/gitlab.yml.example config/gitlab.yml
```

### 4. Start server, resque

```
ruby ~/dev.rb
```

Export display for selenium tests

```
export DISPLAY=:99
```

### 5. Setup dev env

1. Edit .gitconfig
2. Regenerate ssh key

```
ssh-keygen -t rsa
sudo cp /home/gitlab/.ssh/id_rsa.pub /home/git/gitlab.pub
sudo -u git -H sh -c "PATH=/home/git/bin:$PATH; gl-setup -q /home/git/gitlab.pub"
```

### 6. Access web server http://x.x.x.x:9999/

```
Admin login details for webUI
user: admin@local.host
pass: 5iveL!fe
```
